FROM python:3.5.6-alpine

RUN apk add gcc \
    musl-dev \
    libffi-dev \
    openssl-dev
RUN apk upgrade
RUN pip install pipenv
COPY . /app
WORKDIR /app
RUN pipenv install --system --deploy --ignore-pipfile